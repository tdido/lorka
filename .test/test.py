import subprocess,shlex

def run(pdbid,args):
    cmd = f"python lorka.py " + args
    cmd = shlex.split(cmd)
    out_now = subprocess.check_output(cmd).decode()
    with open(f".test/out/{pdbid}.out") as fh:
        out_bef = fh.read()
    assert out_bef == out_now

def test_remote():
    pdbid = "2trx"
    run(pdbid, f"--pdbid {pdbid}")

def test_remote_keep():
    pdbid = "2trx"
    run(pdbid, f"--pdbid {pdbid} --keep")

def test_local_cif():
    pdbid = "2trx"
    run(pdbid, f"--pdbfile .test/pdbs/{pdbid}.cif")

def test_local_cif_gz():
    pdbid = "2trx"
    run(pdbid, f"--pdbfile .test/pdbs/{pdbid}.cif.gz")

def test_local_pdb():
    pdbid = "2trx"
    run(pdbid, f"--pdbfile .test/pdbs/pdb{pdbid}.ent")

def test_local_pdb_gz():
    pdbid = "2trx"
    run(pdbid, f"--pdbfile .test/pdbs/pdb{pdbid}.ent.gz")
