import sys
import os
import gzip
from modeller import model, alignment
from modeller import environ
import shutil

def complete_pdb(env, filename, special_patches=None, transfer_res_num=False,
                 model_segment=None, patch_default=True, model_format="MMCIF"):
    vars = {}
    if model_segment is not None:
        vars['model_segment'] = model_segment
    mdl = model(env, file=filename, model_format=model_format, **vars)
    # Save original chain IDs, since generate_topology resets them
    chain_ids = [c.name for c in mdl.chains]
    aln = alignment(env)
    aln.append_model(mdl, atom_files=filename, align_codes='struc')
    aln.append_model(mdl, atom_files=filename+'.ini', align_codes='struc-ini')
    mdl.clear_topology()
    mdl.generate_topology(aln[-1], patch_default=patch_default)
    if special_patches:
        special_patches(mdl)
    # Save original seq_id, as transfer_xyz sets it
    seq_id = mdl.seq_id
    mdl.transfer_xyz(aln)
    mdl.seq_id = seq_id
    # Restore original chain IDs
    for (chain, chainid) in zip(mdl.chains, chain_ids):
        chain.name = chainid
    mdl.build(initialize_xyz=False, build_method='INTERNAL_COORDINATES')
    if transfer_res_num:
        mdl2 = model(env, file=filename, model_format=model_format, **vars)
        mdl.res_num_from(mdl2, aln)
    return mdl

def fix_structure(fname):
    with open(os.devnull,"w") as nul:
        env = environ()
        env.libs.topology.read('${LIB}/top_heav.lib')
        env.libs.parameters.read('${LIB}/par.lib')

        if ".cif" in fname.lower():
            format = "MMCIF"
        else:
            format = "PDB"

        if fname.endswith(".gz"):
            newfname = fname.rstrip(".gz")
            with gzip.open(fname, 'rb') as f_in:
                with open(newfname, 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            fname = newfname
            os.remove(fname)

        ofname = "{}.fixed.pdb".format(os.path.splitext(fname)[0])
        m = complete_pdb(env, fname, transfer_res_num="true", model_format=format)
        m.write(file=ofname, model_format="PDB")

        sys.stdout = sys.__stdout__

if __name__ == "__main__":
    fname = sys.argv[1]
    fix_structure(fname)
